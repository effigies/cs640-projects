#!/usr/bin/env python
from __future__ import division
from threading import Thread
import time
import numpy as np

import opencv
from opencv import highgui

from Video import VideoThread, Capture, Display, Writer, Controller

class Operator:
    def update(self,frame):
        pass

    def score(self):
        return 0

    def frame(self):
        return 0

class Processor(VideoThread):
    def __init__(self, source, fps=30.0, *operators):
        super(Processor,self).__init__()
        self.source = source
        self.width = self.source.width
        self.height = self.source.height
        self.delay = 1/fps
        self.operators = operators
        frame = self.source.frame
        self.raw = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)
        self.frame = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)

    def process(self):
        opencv.cvCopy(self.source.frame,self.raw)
        for op in self.operators:
            op.update(self.raw)
        self.results = [op.score() for op in self.operators]
        self.frame = self.merge([op.frame for op in self.operators])

    def merge(self,*frames):
        return self.raw

class Filters(Processor):
    results = None

    def process(self):
        opencv.cvCopy(self.source.frame,self.raw)
        tmp = self.raw
        for op in self.operators:
            op.update(tmp)
            tmp = op.frame
        score = op.score()
        if score is not None:
            if self.results is None:
                self.results = np.matrix(op.score())
            else:
                self.results = np.concatenate((self.results,np.matrix(score)))
        self.frame = tmp

class Filter(Operator):
    raw = None
    def update(self,frame):
        if self.raw is None:
            self.raw = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)
        opencv.cvCopy(frame,self.raw)
        self.frame = self.getFrame()

class Grayscale(Filter):
    def getFrame(self):
        frame = opencv.cvCreateMat(self.raw.rows,self.raw.cols,self.raw.type)
        gray = opencv.cvCreateImage(opencv.cvGetSize(self.raw), opencv.IPL_DEPTH_8U, 1)

        opencv.cvCvtColor(self.raw,gray,opencv.CV_BGR2GRAY)
        opencv.cvCvtColor(gray,frame,opencv.CV_GRAY2BGR)
        return frame

class Smooth(Filter):
    def getFrame(self):
        frame = opencv.cvCreateMat(self.raw.rows,self.raw.cols,self.raw.type)
        opencv.cvSmooth(self.raw,frame,opencv.CV_MEDIAN,11)
        return frame

class Diff(Operator):
    def __init__(self):
        self.update = self.initUpdate

    # This is really not very complex; I just didn't want to go through
    # always go through logic that is only needed a couple times forever
    def initUpdate(self,frame):
        self.base = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)
        opencv.cvCopy(frame,self.base)
        self.current = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)
        opencv.cvCopy(frame,self.current)
        self.counter = 1
        self.update = self.midUpdate
        self.frame = self.current

    def midUpdate(self,frame):
        opencv.cvCopy(frame,self.base)
        opencv.cvCopy(frame,self.current)
        self.counter += 1
        self.frame = self.current
        if self.counter > 2:
            self.update = self.lateUpdate

    def lateUpdate(self,frame):
        opencv.cvCopy(frame,self.current)
        self.frame = self.getFrame()

    def getFrame(self):
        return (self.current - self.base) + (self.base - self.current)

class Delta(Operator):
    def update(self,frame):
        self.old = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)
        self.current = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)
        opencv.cvCopy(frame,self.current)
        self.frame = self.getFrame()
        self.update = self.lateUpdate
        
    def lateUpdate(self,frame):
        opencv.cvCopy(self.current,self.old)
        opencv.cvCopy(frame,self.current)
        self.frame = self.getFrame()

    def getFrame(self):
        return (self.current - self.old) + (self.old - self.current)

class Threshold(Filter):
    def __init__(self,level=64,threshold_type=opencv.CV_THRESH_BINARY):
        self.level = level
        self.ttype = threshold_type

    def getFrame(self):
        frame = opencv.cvCreateMat(self.raw.rows,self.raw.cols,self.raw.type)
        opencv.cvThreshold(self.raw,frame,self.level,255,self.ttype)
        return frame

class Trace(Operator):
    def __init__(self,decay=0.95,skip=5):
        self.frame = None
        self.empty = None
        self.decay = decay
        self.skip = skip

    def update(self,frame):
        if self.frame is None:
            self.frame = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)
            self.empty = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)
        if self.skip:
            opencv.cvCopy(frame,self.frame)
            self.skip -= 1
        else:
            opencv.cvScale(self.frame,self.frame,self.decay)
            opencv.cvMax(self.frame,frame,self.frame)
            self.update = self.lateUpdate

    def lateUpdate(self,frame):
        opencv.cvScale(self.frame,self.frame,self.decay)
        opencv.cvMax(self.frame,frame,self.frame)

    def getFrame(self):
        return self.frame

class Moments(Filter):
    def getFrame(self):
        return self.raw

    def score(self):
        frame = opencv.cvCreateImage(opencv.cvGetSize(self.raw), opencv.IPL_DEPTH_8U, 1)
        opencv.cvCvtColor(self.raw,frame,opencv.CV_BGR2GRAY)
        active_pixels = np.sum(frame)/255
        if active_pixels > 0.01*(frame.rows*frame.cols):
            moments = opencv.CvMoments()
            hu = opencv.CvHuMoments()
            opencv.cvMoments(frame,moments,True)
            opencv.cvGetHuMoments(moments, hu)
            centralMoments = np.array([
                opencv.cvGetCentralMoment(moments,x,y)
                for x in range(4)
                for y in range(4-x)
            ])
            huMoments = np.array([hu.hu1,hu.hu2,hu.hu3,hu.hu4,hu.hu5,hu.hu6,hu.hu7])
            return np.concatenate((centralMoments,huMoments))
        else:
            return None

