#!/usr/bin/env python
from __future__ import division
from Video import Capture, Display, Controller, Writer
from Operator import Operator, Processor,Filters
import signal
import sys
import numpy as np

import opencv
from opencv import highgui

class Filter(Operator):
    raw = None
    def update(self,frame):
        if self.raw is None:
            self.raw = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)
        opencv.cvCopy(frame,self.raw)
        self.frame = self.getFrame()

class Grayscale(Filter):
    def getFrame(self):
        frame = opencv.cvCreateMat(self.raw.rows,self.raw.cols,self.raw.type)
        gray = opencv.cvCreateImage(opencv.cvGetSize(self.raw), opencv.IPL_DEPTH_8U, 1)

        opencv.cvCvtColor(self.raw,gray,opencv.CV_BGR2GRAY)
        opencv.cvCvtColor(gray,frame,opencv.CV_GRAY2BGR)
        return frame

class Smooth(Filter):
    def getFrame(self):
        frame = opencv.cvCreateMat(self.raw.rows,self.raw.cols,self.raw.type)
        opencv.cvSmooth(self.raw,frame,opencv.CV_MEDIAN,11)
        return frame

class Diff(Operator):
    def __init__(self):
        self.update = self.initUpdate

    # This is really not very complex; I just didn't want to go through
    # always go through logic that is only needed a couple times forever
    def initUpdate(self,frame):
        self.base = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)
        opencv.cvCopy(frame,self.base)
        self.current = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)
        opencv.cvCopy(frame,self.current)
        self.counter = 1
        self.update = self.midUpdate
        self.frame = self.current

    def midUpdate(self,frame):
        opencv.cvCopy(frame,self.base)
        opencv.cvCopy(frame,self.current)
        self.counter += 1
        self.frame = self.current
        if self.counter > 2:
            self.update = self.lateUpdate

    def lateUpdate(self,frame):
        opencv.cvCopy(frame,self.current)
        self.frame = self.getFrame()

    def getFrame(self):
        return (self.current - self.base) + (self.base - self.current)

class Delta(Operator):
    def update(self,frame):
        self.old = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)
        self.current = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)
        opencv.cvCopy(frame,self.current)
        self.frame = self.getFrame()
        self.update = self.lateUpdate
        
    def lateUpdate(self,frame):
        opencv.cvCopy(self.current,self.old)
        opencv.cvCopy(frame,self.current)
        self.frame = self.getFrame()

    def getFrame(self):
        return (self.current - self.old) + (self.old - self.current)

class Threshold(Filter):
    def __init__(self,level=64,threshold_type=opencv.CV_THRESH_BINARY):
        self.level = level
        self.ttype = threshold_type

    def getFrame(self):
        frame = opencv.cvCreateMat(self.raw.rows,self.raw.cols,self.raw.type)
        opencv.cvThreshold(self.raw,frame,self.level,255,self.ttype)
        return frame

class Trace(Operator):
    def __init__(self,decay=0.95,skip=5):
        self.frame = None
        self.empty = None
        self.decay = decay
        self.skip = skip

    def update(self,frame):
        if self.frame is None:
            self.frame = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)
            self.empty = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)
        if self.skip:
            opencv.cvCopy(frame,self.frame)
            self.skip -= 1
        else:
            opencv.cvScale(self.frame,self.frame,self.decay)
            opencv.cvMax(self.frame,frame,self.frame)
            self.update = self.lateUpdate

    def lateUpdate(self,frame):
        opencv.cvScale(self.frame,self.frame,self.decay)
        opencv.cvMax(self.frame,frame,self.frame)

    def getFrame(self):
        return self.frame

class Moments(Filter):
    def getFrame(self):
        return self.raw

    def score(self):
        frame = opencv.cvCreateImage(opencv.cvGetSize(self.raw), opencv.IPL_DEPTH_8U, 1)
        opencv.cvCvtColor(self.raw,frame,opencv.CV_BGR2GRAY)
        active_pixels = np.sum(frame)/255
        if active_pixels > 0.01*(frame.rows*frame.cols):
            moments = opencv.CvMoments()
            hu = opencv.CvHuMoments()
            opencv.cvMoments(frame,moments,True)
            opencv.cvGetHuMoments(moments, hu)
            centralMoments = np.array([
                opencv.cvGetCentralMoment(moments,x,y)
                for x in range(4)
                for y in range(4-x)
            ])
            huMoments = np.array([hu.hu1,hu.hu2,hu.hu3,hu.hu4,hu.hu5,hu.hu6,hu.hu7])
            return np.concatenate((centralMoments,huMoments))
        else:
            return None

def main(argv):
    # Initialize camera
    camera = highgui.cvCreateCameraCapture(0)
#    camera = highgui.cvCreateFileCapture("test.1.mpg")
    highgui.cvGrabFrame(camera)

    threads = []
    # Initialize threads
    capture = Capture(camera)
    dRaw = Display("Raw",capture)
    threads.extend([capture,dRaw])

    oGray       = Grayscale()
    oDelta      = Delta()
    oThresh     = Threshold()
    oTrace      = Trace(0.95,20)
    oThresh2    = Threshold()

    # The important bit is the order of the operators
    # delta -> threshold -> trace -> threshold
    filters = Filters(capture,30.0,oGray,oDelta,oThresh,oTrace,oThresh2,Moments())
    dFilters = Display("Filters",filters)
    threads.extend([filters,dFilters])

    # Use controller to handle closing of 
    controller = Controller(*threads)
    signal.signal(signal.SIGINT, lambda s,f: controller.stop())
    controller.start()
    while controller.isAlive():
        controller.join(0.1)

    return filters.results

if __name__ == '__main__':
    #sys.exit(main(sys.argv[1:]))
    results = main(sys.argv[1:])

