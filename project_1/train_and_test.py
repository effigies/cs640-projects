#!/usr/bin/env python

from Classifier import Instance, Classifier, Reporter
import Video as V
import Operator as O

import opencv
from opencv import highgui
import signal

class Colorify(O.Filter):
    reporter = None
    scalars = [opencv.cvScalar(255,0,0),
               opencv.cvScalar(0,255,0),
               opencv.cvScalar(0,0,255)]

    def setReporter(self,reporter):
        self.reporter = reporter

    def getFrame(self):
        if self.reporter is None or self.reporter.ranks is None:
            return self.raw
        names = [name for rank,name in self.reporter.ranks]
        index = sorted(names).index(names[0])
        frame = opencv.cvCreateMat(self.raw.rows,self.raw.cols,self.raw.type)
        print index
        opencv.cvAndS(self.raw,self.scalars[index],frame)
        return frame

c = Classifier()
#c.train(["test_movies/nod/test.%d.mpg" % i for i in [1,2,3,4]],
#        ["nod" for i in [1,2,3,4]])
#c.train(["test_movies/wave/test.%d.mpg" % i for i in [1,2,3,4]],
#        ["wave" for i in [1,2,3,4]])
#c.train(["test_movies/head_shake/test.%d.mpg" % i for i in [1,2,3,4]],
#        ["head_shake" for i in [1,2,3,4]])
#c.writeTrainingData('firstfour.json')
c.readTrainingData('firstfour.json') # Load five instances of each gesture

oGray           = O.Grayscale()
oDelta          = O.Delta()
oThresh         = O.Threshold()
oTrace          = O.Trace(0.95,9)
oThresh2        = O.Threshold()
oMoments        = O.Moments()
oColorify       = Colorify()
filters = [oGray,oDelta,oThresh,oTrace,oThresh2,oMoments,oColorify]

#source = highgui.cvCreateFileCapture("test_movies/head_shake/test.7.mpg")
source = highgui.cvCreateCameraCapture(0)
highgui.cvGrabFrame(source)
capture = V.Capture(source)
r = Reporter(c,capture,30.0,*filters)
oColorify.setReporter(r)
dReporter = V.Display("Reporter",r)

controller = V.Controller(capture,r,dReporter)
signal.signal(signal.SIGINT, lambda s,f: controller.stop())
controller.start()
while controller.isAlive():
    controller.join(0.1)
