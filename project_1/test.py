#!/usr/bin/env python
from __future__ import division
from Video import Capture, Display, Controller
import signal
import sys

from opencv import highgui

def main(argv):
    # Initialize camera
    camera = highgui.cvCreateCameraCapture(0)
    highgui.cvGrabFrame(camera)

    # Initialize threads
    capture = Capture(camera)
    display = Display("Test",capture)
    display2 = Display("Test2",capture)

    # Use controller to handle closing of 
    controller = Controller(capture,display,display2)
    signal.signal(signal.SIGINT, lambda s,f: controller.stop())
    controller.start()
    while controller.isAlive():
        controller.join(0.1)

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))

