#!/usr/bin/env python
from __future__ import division
from threading import Thread
import time

import opencv
from opencv import highgui

class VideoThread(Thread):
    """A dummy class to manage the threading model"""
    def __init__(self):
        super(VideoThread,self).__init__()
        self.running = True
        self.delay = 0.01

        self.actions = {}

    def process(self):
        pass

    def run(self):
        while self.running:
            #print "%s running" % repr(self)
            self.process()
            #print "%s sleeping for %0.3fs" % (repr(self),self.delay)
            time.sleep(self.delay)
        self.cleanup()

    def cleanup(self):
        pass

    def stop(self):
        self.running = False

# If there are multiple display objects, they will compete for frames,
# so this simply stores them for access by anybody
class Capture(VideoThread):
    """Generic class to store camera or video frames"""
    def __init__(self, source, fps=30.0):
        super(Capture, self).__init__()
        self.source = source
        self.delay = 1/fps
        self.isCamera = (highgui.cvGetCaptureProperty(source,highgui.CV_CAP_PROP_MODE) == -1)

        frame = highgui.cvQueryFrame(self.source)
        self.frame = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)

        self.rows = self.frame.rows
        self.cols = self.frame.cols
        self.width = self.frame.width
        self.height = self.frame.height

        self.process()

    def process(self):
        frame = highgui.cvQueryFrame(self.source)
        if not frame:
            self.stop()
            return
        opencv.cvCopy(frame,self.frame)
        if not self.isCamera:
            ratio = highgui.cvGetCaptureProperty(self.source,highgui.CV_CAP_PROP_POS_AVI_RATIO)
            if 1 - ratio < 0.001:
                self.stop()

    def cleanup(self):
        if self.isCamera:
            highgui.cvReleaseCapture(self.source)

class Writer(VideoThread):
    """Given a Capture object, write to a video file."""
    def __init__(self,fname,capture, fps=30.0):
        super(Writer, self).__init__()
        self.capture = capture
        self.name = fname
        self.fps = fps
        self.delay = 1/fps

        self.counter = 0

        self.actions = {
            's':        self.switch,
            'V':        self.switch     # Kensington slide dongle
        }

        frame = self.capture.frame
        while frame is None:
            frame = self.capture.frame
        self.frame = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)

        self.open()

    def process(self):
        frame = self.capture.frame
        if frame is not None:
            display = self.modify(frame)
            highgui.cvWriteFrame(self.writer,display)

    def modify(self,frame):
        return frame

    def open(self):
        fformat = highgui.CV_FOURCC('P','I','M','1')
        size = opencv.cvSize(self.capture.width,self.capture.height)
        name = "%s.%d.mpg" % (self.name,self.counter)
        self.writer = highgui.cvCreateVideoWriter(name,fformat,self.fps,size)

    def switch(self):
        highgui.cvReleaseVideoWriter(self.writer)
        self.counter += 1
        self.open()

    def cleanup(self):
        highgui.cvReleaseVideoWriter(self.writer)

class Display(VideoThread):
    """Given a Capture object, display it. Subclass this with a different
    modify function to do change the frame."""
    def __init__(self,name,capture, fps=30.0):
        super(Display, self).__init__()
        self.capture = capture
        self.name = name
        self.delay = 1/fps

        highgui.cvNamedWindow(name)

    def process(self):
        frame = self.capture.frame
        if frame is not None:
            display = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)
            opencv.cvCopy(frame,display)
            highgui.cvShowImage(self.name,display)

    def cleanup(self):
        highgui.cvDestroyWindow(self.name)

class Controller(VideoThread):
    """Run a collection of captures and displays, and handle smooth exits"""
    def __init__(self,*subthreads):
        super(Controller, self).__init__()
        self.subthreads = subthreads
        self.delay = 1/40.0

        # Quit functions
        self.actions = {
            'q':        [self.stop],
            '\x1b':     [self.stop]
        }

        # Raise any subthread actions (iterating through every time caused
        # small but noticeable lag)
        for thread in subthreads:
            for k in thread.actions:
                self.actions[k] = self.actions.get(k,[]) + [thread.actions[k]]

    def start(self):
        for thread in self.subthreads:
            thread.start()
        return super(Controller,self).start()

    def cleanup(self):
        for thread in self.subthreads:
            thread.stop()
            thread.join()

    def process(self):
        """Serves the dual purpose of refreshing displays and quitting on 'q'
        or Escape keypresses."""
        k = highgui.cvWaitKey(2)
        if k in self.actions:
            for action in self.actions[k]:
                action()
        if not all(thread.isAlive() for thread in self.subthreads):
            self.stop()
