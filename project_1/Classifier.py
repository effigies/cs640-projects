#!/usr/bin/env python
from __future__ import division
import opencv
from opencv import highgui
import numpy as np
import json

import Video as V
import Operator as O

class Instance:
    def __init__(self,mu,sigma,classification):
        self.mu = mu
        self.sigma = sigma
        self.classification = classification
    
    def __call__(self,candidate):
        return -np.log(np.abs(self.mu - candidate)/self.sigma)
    
    def __repr__(self):
        return "Instance(%f,%f,%s)" % (self.mu,self.sigma,self.classification)

class Classifier:
    def __init__(self):
        self.instances = {}

    def train(self,videos,classes):
        for video,classification in zip(videos,classes):
            source = highgui.cvCreateFileCapture(video)
            highgui.cvGrabFrame(source)
            threads = []

            capture = V.Capture(source)
            threads.append(capture)

            oGray       = O.Grayscale()	
            oDelta      = O.Delta()
            oThresh     = O.Threshold()	
            oTrace      = O.Trace(0.95,9)
            oThresh2    = O.Threshold()	
            oMoments    = O.Moments()

            filters = O.Filters(capture,30.0,oGray,oDelta,oThresh,oTrace,oThresh2,oMoments)
            threads.append(filters)

            controller = V.Controller(*threads)
            controller.start()
            while controller.isAlive():
                controller.join(0.1)

            for (index,moment) in enumerate(np.asarray(filters.results).T):
                interquartile = np.sort(moment)[len(moment)//4:3*len(moment)//4]
                mu = np.mean(interquartile)
                sigma = np.std(interquartile)
                self.instances.setdefault(index,[]).append(
                    Instance(mu,sigma,classification)
                )

    def classify(self,moments):
        votes = {}
        total = 0
        for (index,moment) in enumerate(moments):
            instances = self.instances[index]
            scores = [instance(moment) for instance in instances]
            for i,(s,inst) in enumerate(sorted(zip(scores,instances))):
                votes[inst.classification] = votes.setdefault(inst.classification,0) + i
                total += i

        return sorted([(v/total,k) for k,v in votes.items()],reverse=True)

    def writeTrainingData(self,filename):
        f = open(filename,'w')
        output = {}
        for key in self.instances:
            tmp = []
            for instance in self.instances[key]:
                inst = {}
                inst['mu'] = instance.mu
                inst['sigma'] = instance.sigma
                inst['classification'] = instance.classification
                tmp.append(inst)
            output[key] = tmp
        f.write(json.dumps(output))
        f.close()

    def readTrainingData(self,filename):
        f = open(filename,'r')
        readdata = json.loads(f.read())
        f.close()
        for key in readdata:
            tmp = self.instances.setdefault(int(key),[])
            for inst in readdata[key]:
                instance = Instance(inst['mu'],inst['sigma'],inst['classification'])
                tmp.append(instance)

class Reporter(O.Processor):
    def __init__(self,classifier,*args):
        self.classifier = classifier
        self.ranks = None
        super(Reporter,self).__init__(*args)

    def process(self):
        opencv.cvCopy(self.source.frame,self.raw)
        tmp = self.raw
        for op in self.operators:
            op.update(tmp)
            tmp = op.frame
            if op.score() is not 0:
                score = op.score()
        print "---------------"
        if score is not None:
            self.ranks = self.classifier.classify(score)
            for (r,n) in self.ranks:
                print "%s\t%0.2f" % (n,r)
        else:
            self.ranks = None
#            print "Not enough active pixels"
        self.frame = tmp
