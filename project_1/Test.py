#!/usr/bin/env python

from Classifier import Instance, Classifier, Reporter
import Video as V
import Operator as O

import opencv
from opencv import highgui
import signal

class Validate(O.Filter):
    reporter = None
    def __init__(self,expected_class):
        self.expected = expected_class
        self.valid = 0
        self.total = 0

    def setReporter(self,reporter):
        self.reporter = reporter

    def getFrame(self):
        if self.reporter is not None and self.reporter.ranks is not None:
            self.total += 1
            names = [name for rank,name in self.reporter.ranks]
            if names[0] == self.expected:
                self.valid += 1
        return self.raw

class Test:
    def __init__(self,classifier,videos,classes):
        self.classifier = classifier
        self.videos = videos
        self.classes = classes

    def test(self,index):
        vid = self.videos[index]
        cls = self.classes[index]

        oGray           = O.Grayscale()
        oDelta          = O.Delta()
        oThresh         = O.Threshold()
        oTrace          = O.Trace(0.95,9)
        oThresh2        = O.Threshold()
        oMoments        = O.Moments()
        oValidate       = Validate(cls)

        filters = [oGray,oDelta,oThresh,oTrace,oThresh2,oMoments,oValidate]

        source = highgui.cvCreateFileCapture(vid)
        highgui.cvGrabFrame(source)
        capture = V.Capture(source)
        r = Reporter(c,capture,30.0,*filters)
        oValidate.setReporter(r)
        
        controller = V.Controller(capture,r)
        signal.signal(signal.SIGINT, lambda s,f: controller.stop())
        controller.start()
        while controller.isAlive():
            controller.join(0.1)

        print "%s - %d Correct / %d total" % (cls,oValidate.valid,oValidate.total)

if __name__ == '__main__':
    c = Classifier()
    c.readTrainingData('firstfour.json') # Load four instances of each gesture

    videos = [
        "test_movies/nod/test.5.mpg",
        "test_movies/nod/test.6.mpg",
        "test_movies/nod/test.7.mpg",
        "test_movies/nod/test.8.mpg",
        "test_movies/wave/test.5.mpg",
        "test_movies/wave/test.6.mpg",
        "test_movies/wave/test.7.mpg",
        "test_movies/wave/test.8.mpg",
        "test_movies/head_shake/test.5.mpg",
        "test_movies/head_shake/test.6.mpg",
        "test_movies/head_shake/test.7.mpg",
        "test_movies/head_shake/test.8.mpg"
    ]

    classes = [
        "nod",
        "nod",
        "nod",
        "nod",
        "wave",
        "wave",
        "wave",
        "wave",
        "head_shake",
        "head_shake",
        "head_shake",
        "head_shake"
    ]

    t = Test(c,videos,classes)
    for i in range(len(videos)):
        t.test(i)
