#!/usr/bin/env python
from __future__ import division
from Video import Capture, Display, Controller, Writer
import signal
import sys

import opencv
from opencv import highgui

class Diff(Display):
    def __init__(self, name, capture):
        super(Diff,self).__init__(name,capture)
        frame = self.capture.frame
        self.old = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)

        self.counter = 0
        self.binary = False

        self.actions = {
            'g':        self.grab,
            'b':        self.toggleBinary
        }

    def modify(self,frame):
        if self.counter < 30:
            self.grab()
            self.counter += 1
        return self.transform((frame - self.old) + (self.old - frame))

    def grab(self):
        opencv.cvCopy(self.capture.frame,self.old)

    def toggleBinary(self):
        self.binary = not self.binary

    def transform(self,img):
        if self.binary:
            opencv.cvThreshold(img,img,100,255,opencv.CV_THRESH_BINARY)
        return img

class Delta(Display):
    def __init__(self, name, capture):
        super(Delta,self).__init__(name,capture)
        frame = self.capture.frame
        self.old = opencv.cvCreateMat(frame.rows,frame.cols,frame.type)

    def modify(self,frame):
        diff = (frame - self.old) + (self.old - frame)
        opencv.cvCopy(frame,self.old)
        return diff

class Laplaced(Display):
    def __init__(self, name, capture):
        super(Laplaced,self).__init__(name,capture)

    def modify(self,frame):
        display = opencv.cvCreateImage(opencv.cvGetSize(frame), opencv.IPL_DEPTH_16S, 3)
        opencv.cvLaplace(frame,display,7)
        return display

def main(argv):
    # Initialize camera
    video = highgui.cvCreateCameraCapture(0)
    highgui.cvGrabFrame(video)

    # Initialize threads
    capture = Capture(video)
    display = Display("Video",capture)
#    gestures = Writer("gesture",capture)
    diff = Diff("Diff",capture)
    delta = Delta("Delta",capture)
    laplaced = Laplaced("Laplaced",capture)

    # Use controller to handle closing and refreshing of windows
    controller = Controller(capture,display,diff,delta,laplaced)
    signal.signal(signal.SIGINT, lambda s,f: controller.stop())
    controller.start()
    while controller.isAlive():
        controller.join(0.1)

    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))

