#!/usr/bin/env python

class Observations:
    def __init__(self,fname):
        f = open(fname,'r')
        contents = filter(None,map(str.split,f.readlines()))
        f.close()
        
        # Ignore first two lines, and grab every other line
        self.seqs = contents[2::2]
