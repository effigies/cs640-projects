#!/usr/bin/env python
import sys
import numpy as np

from HMM import HMM640
from Observation import Observations

def main(argv):
    if len(argv) < 3:
        print "Usage: recognize HMMFILE OBSFILE OUTFILE[S]"
        return 1
    
    hmm_file = argv[0]
    obs_file = argv[1]
    hmm_outs = argv[2:]
    obs = Observations(obs_file)

    if len(obs.seqs) > len(hmm_outs):
        print "%d sequences presented; only %d output filenames given" % (
            len(obs.seqs),len(hmm_outs))
        return 1
    hmm = HMM640(hmm_file)
    
    # Division by zero is handled manually, so don't warn
    np.seterr(invalid='ignore')
    hs = hmm.optimize(obs.seqs)
    for (h,seq,out) in zip(hs,obs.seqs,hmm_outs):
        print "%0.6f %0.6f" % (hmm.recognize(seq),h.recognize(seq))
        h.toFile(out)

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
