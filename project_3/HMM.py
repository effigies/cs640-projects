#!/usr/bin/env python
import numpy as np

eps = np.finfo

class HMM(object):
    def __init__(self,transition,output,initial):

        self.a = transition
        self.b = output
        self.pi = initial

    def forward(self, os):
        """Forward probability of a sequence"""
        alpha = self._forward(os)
        if alpha is None:
            return None
        return alpha[-1,:]

    def _forward(self, os, alpha=None):
        """Forward probability of a sequence and all prefixes"""
        if len(os) == 0:
            return alpha

        o,os = os[0],os[1:]
        if alpha is None:
            return self._forward(os, np.multiply(self.pi,self.b[:,o].T))

        return np.vstack((alpha,
            self._forward(os, np.multiply(alpha*self.a,self.b[:,o].T))
        ))

    def backward(self, os):
        """Backward probability of a sequence"""
        beta = self._backward(os)
        if beta is None:
            return None
        return beta[:,0]

    def _backward(self, os, beta=None):
        """Backward probability of a sequence and all suffixes"""
        if beta is None:
            if len(os) == 0:
                return None
            return self._backward(os, np.ones(self.pi.T.shape))

        o,os = os[-1],os[:-1]
        if len(os) == 0:
            return beta
        return np.hstack((
            self._backward(os, self.a*np.multiply(self.b[:,o],beta)), beta
        ))

    def viterbi(self, os, delta = None, phi = None):
        if len(os) == 0:
            if delta is None:
                return (None,[])
            p = np.max(delta)
            qT = np.argmax(delta)
            return (p,[qT])

        o = os.pop(0)
        if delta is None:
            delta = np.multiply(self.pi,self.b[:,o].T)
            phi = np.zeros(delta.shape)
            return self.viterbi(os, delta, phi)

        temp = np.multiply(delta.T,self.a)
        delta = np.multiply(np.max(temp,0),self.b[:,o].T)
        phi = np.argmax(temp,0)
        p,qs = self.viterbi(os, delta, phi)
        qs.insert(0,phi[0,qs[0]])
        return (p, qs)

    def baumWelch(self, oss):
        hs = []
        alphas = map(self._forward, oss)
        betas = map(self._backward, oss)

        for os, alpha, beta in zip(oss, alphas, betas):
            xis = [xi/np.sum(xi) for xi in (
                np.multiply(self.a,
                    (np.multiply(self.b[:,os[t+1]],beta[:,t+1]) *
                    alpha[t,:]).T)
                for t in range(len(os)-1))]
            gammas = [gamma/np.sum(gamma) for gamma in (
                np.multiply(alpha[t,:].T,beta[:,t]) for t in range(len(os))
                )]
            Xi = np.sum(xis,0)
            Gamma = np.sum(gammas[:-1],0)
            abar = np.divide(Xi,Gamma)
            abar[np.isnan(abar)] = np.array(self.a)[np.isnan(abar)]
            bbar = np.matrix(np.zeros(self.b.shape))
            for t in range(len(os)):
                bbar[:,os[t]] += gammas[t]
            bbar = np.divide(bbar,np.sum(gammas,0))
            bbar[np.isnan(bbar)] = np.array(self.b)[np.isnan(bbar)]
            pibar = gammas[0].T
            pibar[np.isnan(pibar)] = np.array(self.pi)[np.isnan(pibar)]
            hs.append(HMM(abar,bbar,pibar))
        return hs

#        # Failed multiple observation attempt
#        alphas = map(self._forward, oss)
#        betas = map(self._backward, oss)
#        Ps = [np.sum(alpha[-1,:]) for alpha in alphas]
#
#        a = np.matrix(np.zeros(self.a.shape))
#        for i,j in zip(*np.nonzero(np.array(self.a))):
#            for os, alpha, beta, P in zip(oss, alphas, betas, Ps):
#                for t in range(len(os)-1):
#                    a[i,j] += alpha[t,i]*self.a[i,j]*self.b[j,os[t+1]]*beta[j,t+1]/P
#
#        n,m = self.b.shape
#        b = np.matrix(np.zeros((n,m)))
#        for i,j in zip(range(n),range(m)):
#            for os, alpha, beta, P in zip(oss, alphas, betas, Ps):
#                for t in range(len(os)-1):
#                    if os[t] == j:
#                        b[i,j] += alpha[t,i]*beta[i,t]/P
#
#        return HMM(a, b, self.pi[:])

class HMM640(HMM):
    def __init__(self,fname=None):
        if fname:
            return self.fromFile(fname)

    def fromFile(self,fname):
        f = open(fname,'r')
        content = map(str.split,f.readlines())
        f.close()

        N,M,self.T = map(int,content[0])
        self.stateNames = content[1]
        self.symbols = content[2]

        a = np.matrix(content[4:4+N],dtype='float64')
        b = np.matrix(content[5+N:5+2*N],dtype='float64')
        pi = np.matrix(content[6+2*N],dtype='float64')

        super(HMM640,self).__init__(a,b,pi)
        self.symIndex = dict((s,i) for i,s in enumerate(self.symbols))

    def toFile(self,fname):
        f = open(fname,'w')
        N,M = self.b.shape
        f.write('%d %d %d\r\n' % (N, M, self.T))
        f.write(' '.join(self.stateNames) + '\r\n')
        f.write(' '.join(self.symbols) + '\r\n')
        f.write('a:\r\n')
        for row in self.a.tolist():
            f.write(' '.join('%0.6f' % num for num in row) + '\r\n')
        f.write('b:\r\n')
        for row in self.b.tolist():
            f.write(' '.join('%0.6f' % num for num in row) + '\r\n')
        f.write('pi:\r\n')
        f.write(' '.join('%0.6f' % num for num in self.pi.tolist()[0]) + '\r\n')
        f.close()

    def withHMM(self,hmm):
        h = HMM640()
        h.a = hmm.a
        h.b = hmm.b
        h.pi = hmm.pi
        h.T = self.T
        h.stateNames = self.stateNames
        h.symbols = self.symbols
        h.symIndex = self.symIndex
        return h

    def recognize(self, seq):
        return np.sum(self.forward(map(self.symIndex.get,seq)))

    def statepath(self, seq):
        (p, qs) = self.viterbi(map(self.symIndex.get,seq))
        return ' '.join([repr(p)] + [self.stateNames[q] for q in qs if p])

    def optimize(self, seqs):
        oss = map(lambda seq: map(self.symIndex.get,seq),seqs)
        return map(self.withHMM, self.baumWelch(oss))

