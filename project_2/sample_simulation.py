#!/usr/bin/env python
from Datasets import *
from Trainer import Trainer

ct = Trainer('data/crx.data.training','data/crx.data.testing',CRX)
lt = Trainer('data/lenses.training','data/lenses.testing',Lenses)

repeatParams = {
    'minL': 1,
    'maxL': 3,
    'minH': 15,
    'maxH': 15,
    'reps': 1
}

print "CRX"
crxResults = ct.repeatRanges(learning_rate=0.002,**repeatParams)
print "Lenses"
lensResults = lt.repeatRanges(learning_rate=0.01,**repeatParams)

###
### Display learning curves for rep 5, h = 5, L in {3,4,5}
###
ct.displayTraining(*crxResults[0][1][0])
ct.displayTraining(*crxResults[0][2][0])
ct.displayTraining(*crxResults[0][3][0])
lt.displayTraining(*lensResults[0][1][0])
lt.displayTraining(*lensResults[0][2][0])
lt.displayTraining(*lensResults[0][3][0])
