#!/usr/bin/env python
from __future__ import division
import numpy as np

class Perceptron:
    def __init__(self,*weights):
        if len(weights) == 0:
            raise ValueError("Perceptron must have at least one weight matrix")

        self.weights = map(np.matrix,weights)
        for (layer1,layer2) in zip(self.weights,self.weights[1:]):
            if layer1.shape[1] != layer2.shape[0]:
                raise ValueError("Rows of a weight matrix must match the " \
                    "number of columns in the preceding matrix.")

        self.bias = np.zeros(weights[-1].shape[1])

    def sigmoid(self,sigma):
        return 1/(1+np.exp(-sigma))

    def dsigmoid(self,sigma):
        s = self.sigmoid(sigma)
        return np.multiply(s,1-s)
    
    def evaluate(self,inputs):
        layer = inputs
        if layer.shape[1] != self.weights[0].shape[0]:
            raise ValueError("Rows of input must match the columns in the " \
                "initial weight matrix")
        for weight in self.weights:
            layer = self.sigmoid(layer * weight)
        return layer
