#!/usr/bin/env python
from __future__ import division
from Perceptron import Perceptron
import numpy as np

class BackProp(Perceptron):
    def __init__(self,*shapes):
        weights = []
        for shape in shapes:
            weights.append(np.random.uniform(-1,1,shape))

        Perceptron.__init__(self,*weights)

    def _validate(self,instances,targets):
        if instances.shape[0] != targets.shape[0]:
            raise ValueError("There must be a target for each instance.")
        if instances.shape[1] != self.weights[0].shape[0]:
            raise ValueError("Rows of input must match the columns in the " \
                "initial weight matrix")
        if targets.shape[1] != self.weights[-1].shape[1]:
            raise ValueError("Number of columns in targets must match that " \
                "of final weight matrix")

    def train(self,instances,targets,eta):
        self._validate(instances,targets)

        nets = []
        activations = [instances]

        # The output of each layer is simply the thresholded product of the
        # previous layer and the weights between layers
        output = instances
        net = instances
        for weight in self.weights:
            nets.append(net)
            net = output * weight
            output = self.sigmoid(net)
            activations.append(output)

        delta = targets - output

        deltas = []
        for (net,weight) in zip(nets[::-1],self.weights[::-1]):
            deltas.append(delta)
            delta = np.multiply(delta*weight.T,self.dsigmoid(net))

        self.old_weights = [np.copy(weights) for weights in self.weights]

        for (activation,delta,weights) in zip(activations,deltas[::-1],self.weights):
            weights += eta*activation.T*delta

    def backtrack(self):
        self.weights = self.old_weights

    def loss2(self,delta):
        return np.sum(np.multiply(delta,delta))/delta.shape[0]

    def error(self,instances,targets,loss=loss2):
        self._validate(instances,targets)

        output = instances
        for weight in self.weights:
            output = self.sigmoid(output * weight)

        return loss(self,targets - output)

    def classify(self,instances):
        output = self.evaluate(instances)
        return np.double(output == np.max(output,1)*np.ones((1,output.shape[1])))
