#!/usr/bin/env python
from __future__ import division
import numpy as np

class Type(object):
    optional = False

class Categorical(Type):
    def __init__(self):
        self.categories = {}

    def set(self, options):
        options = list(set(options))
        if '?' in options:
            self.optional = True
            options.remove('?')
            self.none = np.matrix(np.zeros(len(options)))

        self.options = options
        basis = np.matrix(np.eye(len(options)))
        for i,opt in enumerate(options):
            self.categories[opt] = basis[i,:]

    def encode(self, value):
        if self.optional and value == '?':
            return self.none
        return self.categories[value]

    def decode(self, code):
        return self.options[code]

class Continuous(Type):
    def __init__(self):
        self.mu = 0
        self.sigma = 0

    def set(self, values):
        if '?' in values:
            self.optional = True
            values = values[values != '?']

        doubles = np.double(values)

        self.mu = np.mean(doubles)
        self.sigma = np.std(doubles)

    def encode(self,value):
        if self.optional and value == '?':
            return np.matrix([[0]])
        return np.matrix([[(np.double(value) - self.mu)/self.sigma]])

    def decode(self, code):
        return self.mu + self.sigma*code

class Dataset:
    _training = []
    _testing = []
    types = []
    class_type = []

    def initialize_types(self):
        for (col,t) in zip(self._training.T,self.types):
            t.set(col)

    def convert(self,dataset):
        ret = np.matrix(np.ones((dataset.shape[0],1)))
        for (col,t) in zip(dataset.T,self.types):
            new_col = np.concatenate([t.encode(c) for c in col])
            ret = np.concatenate((ret,new_col),1)
        targets = np.concatenate([self.class_type.encode(c) for c in dataset[:,-1]])

        return (ret,targets)

    def training(self):
        return self.convert(self._training)

    def testing(self):
        return self.convert(self._testing)

class CRX(Dataset):
    def __init__(self,training,testing):
        self._training = training
        self._testing = testing

        self.types = [Categorical(), # A1
                      Continuous(),  # A2
                      Continuous(),  # A3
                      Categorical(), # A4
                      Categorical(), # A5
                      Categorical(), # A6
                      Categorical(), # A7
                      Continuous(),  # A8
                      Categorical(), # A9
                      Categorical(), # A10
                      Continuous(),  # A11
                      Categorical(), # A12
                      Categorical(), # A13
                      Continuous(),  # A14
                      Continuous()]  # A15

        self.types[0].set(training[:,0])
        self.types[1].set(training[:,1])
        self.types[2].set(training[:,2])
        self.types[3].set(training[:,3])
        self.types[4].set(training[:,4])
        self.types[5].set(training[:,5])
        self.types[6].set(training[:,6])
        self.types[7].set(training[:,7])
        self.types[8].set(training[:,8])
        self.types[9].set(training[:,9])
        self.types[10].set(training[:,10])
        self.types[11].set(training[:,11])
        self.types[12].set(training[:,12])
        self.types[13].set(training[:,13])
        self.types[14].set(training[:,14])

        self.class_type = Categorical()
        self.class_type.set(['+','-'])

class Lenses(Dataset):
    def __init__(self,training,testing):
        self._training = training
        self._testing = testing

        self.types = [Categorical(),
                      Categorical(),
                      Categorical(),
                      Categorical()]

        self.types[0].set(['1','2','3'])
        self.types[1].set(['1','2'])
        self.types[2].set(['1','2'])
        self.types[3].set(['1','2'])

        self.class_type = Categorical()
        self.class_type.set(['1','2','3'])
