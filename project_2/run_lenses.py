#!/usr/bin/env python
from Datasets import Lenses
from Trainer import Trainer

t = Trainer('data/lenses.training','data/lenses.testing',Lenses)
errors = t.sortedSingleLayer(1,t.nfeatures,0.01)

for error in errors:
    t.displayTraining(*error)
