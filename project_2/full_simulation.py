#!/usr/bin/env python
from Datasets import *
from Trainer import Trainer

ct = Trainer('data/crx.data.training','data/crx.data.testing',CRX)
lt = Trainer('data/lenses.training','data/lenses.testing',Lenses)

repeatParams = {
    'minL': 1,
    'maxL': 3,
    'minH': 1,
    'reps': 1
}

print "CRX"
crxResults = ct.repeatRanges(maxH=2*ct.nfeatures,learning_rate=0.002,**repeatParams)
print "Lenses"
lensResults = lt.repeatRanges(maxH=2*lt.nfeatures,learning_rate=0.01,**repeatParams)

###
### Display learning curves for rep 5, h = 5, L in {3,4,5}
###
ct.displayTraining(*crxResults[5][1][14])
ct.displayTraining(*crxResults[5][2][14])
ct.displayTraining(*crxResults[5][3][14])
lt.displayTraining(*lensResults[5][1][14])
lt.displayTraining(*lensResults[5][2][14])
lt.displayTraining(*lensResults[5][3][14])

###
### Plot minimum test errors
###
crxMinTestErrorsRev = dict([(key,np.array([sorted(rep[key],cmp=lambda (p1,tr1,te1,a1), (p2,tr2,te2,a2): cmp(te1[-1],te2[-1]))[0][2][-1] for rep in crxResults])) for key in crxResults[0]])

ps = pyplot.hist(np.vstack((crxMinTestErrorsRev[1],crxMinTestErrorsRev[2],crxMinTestErrorsRev[3])).T,label=["Single layer","Two hidden layers","Three hidden layers"])
leg = pyplot.legend()
pyplot.xlabel("Minimum Final Test Error")
pyplot.ylabel("Number of Trials")
pyplot.show()

###
### Plot h for minimum test errors
###
crxMinHRev = dict([(key,np.array([sorted(rep[key],cmp=lambda (p1,tr1,te1,a1), (p2,tr2,te2,a2): cmp(te1[-1],te2[-1]))[0][0].weights[0].shape[1] for rep in crxResults])) for key in crxResults[0]])

ps = pyplot.hist(np.vstack((crxMinHRev[1],crxMinHRev[2],crxMinHRev[3])).T,label=["Single layer","Two hidden layers","Three hidden layers"])
leg = pyplot.legend()
pyplot.xlabel("Number of Nodes")
pyplot.ylabel("Number of Trials")
pyplot.show()

###
### Plot minimum test errors
###
lensMinTestErrorsRev = dict([(key,np.array([sorted(rep[key],cmp=lambda (p1,tr1,te1,a1), (p2,tr2,te2,a2): cmp(te1[-1],te2[-1]))[0][2][-1] for rep in lensResults])) for key in lensResults[0]])

ps = pyplot.hist(np.vstack((lensMinTestErrorsRev[1],lensMinTestErrorsRev[2],lensMinTestErrorsRev[3])).T,label=["Single layer","Two hidden layers","Three hidden layers"])
leg = pyplot.legend()
pyplot.xlabel("Minimum Final Test Error")
pyplot.ylabel("Number of Trials")
pyplot.show()

###
### Plot h for minimum test errors
###
lensMinHRev = dict([(key,np.array([sorted(rep[key],cmp=lambda (p1,tr1,te1,a1), (p2,tr2,te2,a2): cmp(te1[-1],te2[-1]))[0][0].weights[0].shape[1] for rep in lensResults])) for key in lensResults[0]])

ps = pyplot.hist(np.vstack((lensMinHRev[1],lensMinHRev[2],lensMinHRev[3])).T,label=["Single layer","Two hidden layers","Three hidden layers"])
leg = pyplot.legend(loc=2)
pyplot.xlabel("Number of Nodes")
pyplot.ylabel("Number of Trials")
pyplot.show()
