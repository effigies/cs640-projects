#!/usr/bin/env python
from Datasets import CRX
from Trainer import Trainer

t = Trainer('data/crx.data.training','data/crx.data.testing',CRX)
errors = t.sortedSingleLayer(1,t.nfeatures,0.002)

for error in errors:
    t.displayTraining(*error)
