#!/usr/bin/env python
from Datasets import *
from BackProp import BackProp
import csv
try:
	from mpl_toolkits.axes_grid1 import host_subplot
	HOSTSUBPLOT = True
except:
	HOSTSUBPLOT = False
from matplotlib import pyplot

class Trainer:
    def __init__(self,train_path,test_path,dataClass):
        """Read training and test data from files and preprocess according to
        a class."""
        train_file = open(train_path)
        train_reader = csv.reader(train_file)
        training_data = np.array(list(train_reader))
        train_file.close()
        
        test_file = open(test_path)
        test_reader = csv.reader(test_file)
        testing_data = np.array(list(test_reader))
        test_file.close()

        preprocessor = dataClass(training_data,testing_data)
        
        # Extract training and test features and targets
        self.train_features,self.train_targets = preprocessor.training()
        self.test_features,self.test_targets = preprocessor.testing()

        # Useful constants
        self.nfeatures = self.train_features.shape[1]
        self.noutputs = self.train_targets.shape[1]
        self.actual_classes = np.nonzero(self.test_targets)[1]

    def __call__(self, hidden_layers, learning_rate):
        """Perform backprop learning on a network with hidden layers"""
        layer0 = (self.nfeatures,hidden_layers[0])
        layerL = (hidden_layers[-1],self.noutputs)
        layers = [layer0] + zip(hidden_layers,hidden_layers[1:]) + [layerL]

        p = BackProp(*layers)
    
        test_errors = np.array([p.error(self.test_features,self.test_targets)])
        train_errors = np.array([p.error(self.train_features,self.train_targets)])
        accuracy = np.array([])
        
        for steps in range(250):
            classifications = np.nonzero(p.classify(self.test_features))[1]
            hits = np.array([np.array(classifications == self.actual_classes)])[0]
            accuracy = np.concatenate((accuracy,np.array([np.mean(hits)])))

            p.train(self.train_features,self.train_targets,learning_rate)
        
            test_error = np.array([p.error(self.test_features,self.test_targets)])
            train_error = np.array([p.error(self.train_features,self.train_targets)])
        
            if test_error > test_errors[-1]:
                p.backtrack()
                break
        
            train_errors = np.concatenate((train_errors,train_error),1)
            test_errors = np.concatenate((test_errors,test_error),1)

        return (p,train_errors,test_errors,accuracy)

    def sortedSingleLayer(self,minimum,maximum,learning_rate):
        errors = []
        for nhidden in range(minimum,2*maximum+1):
            errors.append(self([nhidden],learning_rate))
        errors.sort(cmp=lambda (p1,tr1,te1,a1), (p2,tr2,te2,a2): cmp(te1[-1],te2[-1]))
        return errors

    def repeatedSingleLayer(self,minimum,maximum,learning_rate,repetitions):
        optHidden = []
        minTrainError = []
        minTestError = []
        maxTestAccuracy = []
        for i in range(repetitions):
            errors = self.sortedSingleLayer(minimum,maximum,learning_rate)
            (p,train_errors,test_errors,accuracy) = errors[0]

            optHidden.append(p.weights[0].shape[1])
            minTrainError.append(train_errors[-1])
            minTestError.append(test_errors[-1])
            maxTestAccuracy.append(np.max(accuracy))

        return (optHidden,minTrainError,minTestError,maxTestAccuracy)

    def repeatRanges(self,minL,maxL,minH,maxH,reps,learning_rate):
        """Train for a range of layers and a range of hidden nodes per layer,
        repeated"""
        results = []
        for i in range(reps):
            print "Repetition %d" % i
            rep = {}
            for nLayers in range(minL,maxL+1):
                print "Layers: %d" % nLayers
                tmp = []
                for nHidden in range(minH,maxH+1):
                    print "Nodes: %d" % nHidden
                    tmp.append(self(np.repeat(nHidden,nLayers),learning_rate))
                rep[nLayers] = tmp
            results.append(rep)
        return results

    def displayTraining(self,p,train_errors,test_errors,accuracy):
	if HOSTSUBPLOT:
            host = host_subplot(111)
            par = host.twinx()
            host.set_xlabel("Training Trial")
            host.set_ylabel("Error")
            host.set_ylim(0,1.4)
            par.set_ylabel("Misclassification Rate")
            par.set_ylim(0,1)
    
            p1, = host.plot(np.arange(len(test_errors)),train_errors,label="Training Error")
            p2, = host.plot(np.arange(len(test_errors)),test_errors,label="Testing Error")
            p3, = par.plot(np.arange(len(test_errors)),1-accuracy,label="Misclassification Rate")
    
        else:
            # Lab machines don't have host_subplot, so adjust the ylim and
            # ylabels
            p1, = pyplot.plot(np.arange(len(test_errors)),train_errors,label="Training Error")
            p2, = pyplot.plot(np.arange(len(test_errors)),test_errors,label="Testing Error")
            p3, = pyplot.plot(np.arange(len(test_errors)),1-accuracy,label="Misclassification Rate")
		
            pyplot.ylim(0,1)
            pyplot.xlabel("Training Trial")
            pyplot.ylabel("Error/Misclassification Rate")

        leg = pyplot.legend()
    
        leg.texts[0].set_color(p1.get_color())
        leg.texts[1].set_color(p2.get_color())
        leg.texts[2].set_color(p3.get_color())

        pyplot.show()
