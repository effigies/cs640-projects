#include <opencv/highgui.h>
#include "CS440P0.h"

CS440P0::CS440P0(){
    myChar = 0;
    
    namedWindow("processed",1);
}

bool CS440P0::doWork(Mat& frame)
{
    processed(frame);
    
    // Replace colors with average luminance, producing gray-scale image
    for (int x = 0; x < processed.getWidth(); x++) {
        for (int y = 0; y < processed.getHeight(); y++) {
            Color c = processed.get(x,y);
            int avg = (c.r + c.g + c.b) / 3;
            processed.set(x,y,avg,avg,avg);
        }
    }

    /*
    // Place 20x20 box of red green or blue in the upper left corner
    for(int x=0; x<20; x++)
    {
        for(int y=0; y<20; y++)
        {
            if(myChar == 'r')
                processed.set(x,y,255,0,0);
            if(myChar == 'g')
                processed.set(x,y,0,255,0);
            if(myChar == 'b')
                processed.set(x,y,0,0,255);
        }
    }
    */
    
    //show the processed image
    imshow("processed", processed.getImage());
    cvWaitKey(30);
    
    return true;
}

void CS440P0::setKey(char c)
{
	myChar = c;
}
