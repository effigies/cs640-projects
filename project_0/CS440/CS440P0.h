#ifndef _CS440P0_H
#define _CS440P0_H

#include <cv.h>
#include "Image.h"

class CS440P0
{
public:
	CS440P0();

	bool doWork(Mat& frame);
	void setKey(char c);
	Image processed;
	char myChar;
};

#endif
