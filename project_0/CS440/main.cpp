//hello world

#include <iostream>

#include <cv.h>
#include <highgui.h>
using namespace cv;

#include "CS440P0.h"
//#include "Image.h"

#include <iostream>
using std::cout;
using std::endl;


int main(int argc, char* argv[])
{
	
    VideoCapture cap(0); // open the default camera
    if(!cap.isOpened())  // check if we succeeded
        return -1;

	CS440P0 homework;

    namedWindow("video",1);
    for(;;)
    {
        Mat frame;
        cap >> frame; // get a new frame from camera
		//flip(frame,frame,0);

        imshow("video", frame);

		homework.doWork(frame);

		char c = waitKey(30);
		if ('a' <= c && c <= 'z')
		{
			cout << "the input key is " << c << endl;
		}
		if(c == 'q') break;
		else if(c != -1)
		{
			homework.setKey(c);
		}
    }
    // the camera will be deinitialized automatically in VideoCapture destructor
	
    return 0;
}